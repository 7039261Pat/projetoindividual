FROM alpine

LABEL maintainer="Patrícia Machado"

COPY dados.xml/ /myapp/

WORKDIR /myapp

CMD [ "cat","dados.xml" ]